<?php

use oat\tao\helpers\Template;

Template::inc('form_context.tpl', 'tao');
?>

<script language="javascript">
    function printdiv(printpage) {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>


<div class="main-container tao-scope">
    <div>
        <br>
        <button onClick="printdiv('div_print');" value=" Print ">Print this page</button>
        <a href="<?= $downloadJSONLink ?>" target="_blank" class="button">Download JSON</a>
        <a href="<?= $downloadCSVLink ?>" target="_blank" class="button">Download CSV</a>
    </div>

    <div id="div_print">
        <style>
            #test-taker {
                font-family: Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            #test-taker td,
            #customers th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #test-taker tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #test-taker tr:hover {
                background-color: #ddd;
            }

            #test-taker th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #04AA6D;
                color: white;
            }

            .button {
                background-color: #4CAF50;
                /* Green */
                border: none;
                color: white;
                padding: 5px 10px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
            }
        </style>

        <h1>Patch Generator Plugin</h1>
        <h2>Name: <b><?= $TestTakerClassLabel ?></b></h2>
        <table id="test-taker">
            <tr>
                <th>URI</th>
                <th>Login Name</th>
                <th>Password</th>
            </tr>
            <?php
            foreach ($TestTakersData as $TestTakerData) {
                echo "<tr>";
                echo "<td>" . $TestTakerData['uri'] . "</td>";
                echo "<td>" . $TestTakerData['login-name'] . "</td>";
                echo "<td>" . $TestTakerData['password'] . "</td>";
                echo "</tr>";
            }
            ?>
        </table>

    </div>
</div>
<?php
if (has_data('test')) {
    echo "Dump";
}
?>
<script>
    // setTimeout(function(){
    //     window.location.reload(true);
    // }, 3000);
</script>
<?php Template::inc('footer.tpl', 'tao'); ?>
