# Tao Test-Takers Patch Generator V0.02 

[![N|Sayegh1944](http://www.sayegh1944.com/images/logo.png)](http://www.sayegh1944.com/)


NEED TO ADD GITIGNORE for Files deirectory



You Can Create a patch users and automatically assign them to group.
You Can then download and print users list.
And After finish you can delete and the extension will automatically remove all relative data.
---

## User Will Input the following:
1. (Group \ Label) name
2. Test-Takers Users Numbers
	
--- 
## Steps For Algorithm:
1. [X] Find or Create if note exist Groups *New Class* in (Groups), (Test-takers) called ("Patch_Tt_Generator")
2. [X] Create Group in (Groups) Called (GroupName, input1) append (TIMESTAMP, STRING) append ("_group")
3. [X] Create Group in (test-taker) Called (GroupName, input1) append (TIMESTAMP, STRING) append ("_test_takers_class")
4. [X] Create test-takers as following for n (n = input3):
		- label & login : (GroupName, input1) append (COUNTER, STRING"18Char*4Repeats").
		- password: Generated based on Available Charset.

5. [X] Generate JSON File
6. [X] Browse All generated Patches.
7. [X] JSON or CSV file.
8. [X] Delete Group and Test-taker with relative data.
9. [X] Test The Extension on fresh project.

---
			
## Tips For Choosing Group name as following:
```year_school_subject```
	
---

	
## V0.03 Features:
	1- Clean Data for group:
		a- Archive Data for download.
		b- Delete Data after download as following
			1- Assoccisated Results for selected group
			2- Assoccisated Deliveries for selected group						
			3- Assoccisated Test-takers in group.
			4- Test Takers in group.
			5- Group.
	2- Add More Users to existing (Patch Group)
	4- Printable PDF For Username and passwords
	5- validate input data
    6- UI Language (Need Reterival from DB)🤕️
    7- Download Files
    8- Refresh The tree after delete
    9- Select Test to automatically generate delivery and assign them to group. 
    10- Refactor Code. 
    10- Documentation the code.
	11- spell checking code and README file. 

---
### Available Charset: 
	1- For Passwords [0123456789wrfhxzWRFHLXZ] 23 Char
---
## Usage

1. Download `taoDevTools` extension using composer `composer require oat-sa/extension-tao-devtools:3.4.0` then enable extenssision.
2. Edit the other `composer.json` at the root level of your TAO installation. There are 2 parts you will need to add:
At the top level, a new repository definition:

```
    "repositories": [
        {
            "type": "git",
            "url": "https://gitlab.com/sayegh1944/taotesttakerspatchgenerator.git"
        }
    ],
```
And within the existing require block:

```
    "require" : {
        // long list of other repos,
	"sayegh1944/taotesttakerspatchgenerator": "dev-main"
    }

```
3. You are now finally ready to run `composer update` which should find your package on Github and pull it in.


4. Go to Extenision manager and install **taoTestTakersPatchGenerator**  extenision.
   
