<?php

/**  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Copyright (c) 2022 (original work) Sayegh1944;
 *               
 * 
 */

namespace oat\taoTestTakersPatchGenerator\controller;



use oat\generis\model\OntologyAwareTrait;
use oat\oatbox\event\EventManagerAwareTrait;
// use oat\tao\helpers\ApplicationHelper;
use oat\tao\helpers\UserHelper;
use oat\tao\model\event\UserUpdatedEvent;
// use oat\tao\model\security\xsrf\TokenService;
// use oat\tao\model\user\UserLocks;
// use oat\oatbox\user\UserLanguageServiceInterface;
// use oat\oatbox\log\LoggerAwareTrait;
// use oat\oatbox\log\tao_actions_form_Users;

// use oat\taoDeliveryRdf\model\CompileDelivery;
// use oat\taoDeliveryRdf\model\DeliveryAssemblyService;
// use oat\taoDeliveryRdf\model\DeliveryFactory;
use core_kernel_classes_Resource;

// use oat\generis\model\OntologyRdf;
use oat\generis\model\OntologyRdfs;


use core_kernel_classes_Class;
use oat\tao\model\TaoOntology;
use core_kernel_classes_Property;
// use core_kernel_users_Service;


// use oat\oatbox\event\EventManager;

// use oat\taoTestTaker\models\events\TestTakerUpdatedEvent;
// use oat\generis\Helper\UserHashForEncryption;
use oat\generis\model\GenerisRdf;
use oat\generis\model\user\UserRdf;
// use common_session_SessionManager;
use tao_models_classes_LanguageService;
// use tao_models_classes_dataBinding_GenerisFormDataBinder;
// use oat\taoTestTaker\models\events\TestTakerRemovedEvent;

// use tao_helpers_form_elements_xhtml_Textbox;
// use tao_helpers_form_FormFactory;
use oat\taoTestTakersPatchGenerator\controller\PatchTestTakersGeneratorForm as PatchTestTakersGeneratorForm;
use oat\taoTestTakersPatchGenerator\controller\PatchGeneratorHelpers as PatchGeneratorHelpers;
// use tao_actions_SaSModule;

// use oat\taoGroups\models\GroupsService;
// use oat\tao\model\routing\Resolver;
// use common_http_Request;
// use oat\tao\model\accessControl\ActionResolver;
use helpers_File;
// use tao_helpers_Uri;
use Context;

/**
 * Sample controller
 *
 * @author Sayegh1944
 * @package taoTestTakersPatchGenerator
 * @license GPL-2.0
 *
 */
class TaoTestTakersPatchGenerator extends \tao_actions_CommonModule
{
    use OntologyAwareTrait;
    use EventManagerAwareTrait;

    const PROPERTY_MEMBERS_URI = 'http://www.tao.lu/Ontologies/TAOGroup.rdf#member';

    /**
     * initialize the services
     */
    public function __construct()
    {
        parent::__construct();

        $this->defaultData();
    }

    /**
     * A possible entry point to tao
     */
    public function index()
    {
        echo "Please Choose A Patch";
    }

    public function allPatchesGroupJSON()
    {
        $data = array(
            'data'  => ucfirst("Patches"),
            'attributes' => array(
                'id' =>  "default",
                'class' => 'node-class'
            ),
        );

        $className = PatchGeneratorHelpers::$className;

        $groupClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_SUBJECT);
        $subClasses = $groupClass->getSubClasses(true);
        foreach ($subClasses as $subClass) {
            if ($subClass->getLabel() == $className) {
                $subClassClassess = $subClass->getSubClasses(true);
                if (count($subClassClassess) > 0) {
                    $data['children'] = array();
                }
                foreach ($subClassClassess as $subClassClass) {
                    $data['children'][] =  array(
                        'data'  => $subClassClass->getLabel(),
                        'attributes' => array(
                            'id' =>  $subClassClass->getURI(),
                            'class' => 'node-instance'
                        )
                    );
                }
            }
        }


        echo json_encode($data);
    }

    public function parseJSON()
    {
        if ($this->hasRequestParameter('uri')) {
            $TestTakerURI = $this->getRequestParameter('uri');

            $TestTakerClass = new core_kernel_classes_Class($TestTakerURI);
            $TestTakerClassLabel = $TestTakerClass->getLabel();
            $FileDirectory = dirname(__FILE__) . "/../Files";

            $JSONFileName = $FileDirectory . '/' . $TestTakerClassLabel . '.json';


            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$JSONFileName");
            header("Content-Type: application/zip");
            header("Content-Transfer-Encoding: binary");

            // read the file from disk
            readfile($JSONFileName);
        }
    }

    public function downloadJSON()
    {
        if (isset($_GET['uri'])) {
            if (!empty($_GET['uri'])) {
                $TestTakerURI = $this->getRequestParameter('uri');

                $TestTakerClass = new core_kernel_classes_Class($TestTakerURI);
                $TestTakerClassLabel = $TestTakerClass->getLabel();
                $FileDirectory = dirname(__FILE__) . "/../Files";

                $JSONFileName = $FileDirectory . '/' . $TestTakerClassLabel . '.json';
                $data = file_get_contents($JSONFileName);

                header("Cache-Control: public");
                header("Content-Description: File Transfer");
                header("Content-Disposition: attachment; filename=testTakerInfo.json");
                header("Content-Type: application/json");
                header("Content-Transfer-Encoding: binary");


                // read the file from disk
                echo ($data);
            }
        }
    }

    public function downloadCSV()
    {
        if (isset($_GET['uri'])) {
            if (!empty($_GET['uri'])) {
                $TestTakerURI = $this->getRequestParameter('uri');

                $TestTakerClass = new core_kernel_classes_Class($TestTakerURI);
                $TestTakerClassLabel = $TestTakerClass->getLabel();
                $FileDirectory = dirname(__FILE__) . "/../Files";

                $JSONFileName = $FileDirectory . '/' . $TestTakerClassLabel . '.csv';
                header("Cache-Control: public");
                header("Content-Description: File Transfer");
                header("Content-Disposition: attachment; filename=testTakerInfo.csv");
                header("Content-Type: text/csv");
                header("Content-Transfer-Encoding: binary");
                if ($file = fopen($JSONFileName, "r")) {
                    while (!feof($file)) {
                        $line = fgets($file);
                        echo ("$line");
                    }
                    fclose($file);
                }
            }
        }
    }

    public function parseCSV()
    {
        if ($this->hasRequestParameter('uri')) {
            $TestTakerURI = $this->getRequestParameter('uri');

            $TestTakerClass = new core_kernel_classes_Class($TestTakerURI);
            $TestTakerClassLabel = $TestTakerClass->getLabel();
            $FileDirectory = dirname(__FILE__) . "/../Files";

            $JSONFileName = $FileDirectory . '/' . $TestTakerClassLabel . '.csv';


            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$JSONFileName");
            header("Content-Type: application/zip");
            header("Content-Transfer-Encoding: binary");

            // read the file from disk
            readfile($JSONFileName);
        }
    }

    public function deletePatch()
    {
        if ($this->hasRequestParameter('uri')) {
            $TestTakerURI = $this->getRequestParameter('uri');

            $TestTakerClass = new core_kernel_classes_Class($TestTakerURI);

            $TestTakers = new core_kernel_classes_Class(TaoOntology::CLASS_URI_SUBJECT);

            $ArraySearchParams = array(
                'http://www.w3.org/1999/02/22-rdf-syntax-ns#type' => $TestTakerClass->getURI(),
            );

            $TestTakersResources = $TestTakers->searchInstances($ArraySearchParams, array(
                'recursive' => true
            ));

            $FileDirectory = dirname(__FILE__) . "/../Files";

            helpers_File::remove($FileDirectory . '/' . $TestTakerClass->getLabel() . '.json');
            helpers_File::remove($FileDirectory . '/' . $TestTakerClass->getLabel() . '.csv');

            foreach ($TestTakersResources as $TestTakersResource) {
                $TestTakersResource->delete(true);
            }
            $TestTakerClass->delete(true);

            $groupClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_GROUP);

            $GroupResources = $groupClass->searchInstances(array(
                RDFS_LABEL => $TestTakerClass->getLabel(),
            ), array(
                'recursive' => true
            ));

            foreach ($GroupResources as $GroupResource) {
                $GroupResource->delete(true);
            }

            $data = [
                'success' => true,
                'message' => 'Results deleted successfully',
            ];
            $this->setData('reload', true);

            $this->returnJson($data);
        }
    }

    public function getPatcheInfo()
    {
        $serviceLocator = $this->getServiceManager();

        if ($this->hasRequestParameter('uri')) {
            $TestTakerURI = $this->getRequestParameter('uri');
            $TestTakerClass = new core_kernel_classes_Class($TestTakerURI);
            $TestTakerClassLabel = $TestTakerClass->getLabel();
            $FileDirectory = dirname(__FILE__) . "/../Files";

            $JSONFileName = $FileDirectory . '/' . $TestTakerClassLabel . '.json';


            $data = file_get_contents($JSONFileName);
            $TestTakersData = json_decode($data, true);

            $URL = $_SERVER['REQUEST_URI'];
            // die('<pre>'.var_export($URL).'</pre>');

            $URL = explode('/', $URL);
            array_pop($URL);
            $URL = implode('/', $URL);
            $URL = rtrim(ROOT_URL, "/") . $URL . '/';
            $downloadJSONLink = $URL . 'downloadJSON?uri=' . urlencode($TestTakerURI);
            $downloadCSVLink = $URL . 'downloadCSV?uri=' . urlencode($TestTakerURI);


            $this->setData('TestTakerClassLabel', $TestTakerClassLabel);
            $this->setData('downloadJSONLink', $downloadJSONLink);
            $this->setData('downloadCSVLink', $downloadCSVLink);
            $this->setData('TestTakersData', $TestTakersData);
            $this->setView('TaoTestTakersPatchGenerator/testTakerData.tpl');
        }
    }

    public function Create()
    {
        $context = Context::getInstance();

        $Extension = $context->getExtensionName();
        $module = $context->getModuleName();
        $Action = $context->getActionName();


        // die($Extension);
        $FileDirectory = dirname(__FILE__) . "/../Files";

        //instantaite our container
        $PatchTestTakersGeneratorContainer = new PatchTestTakersGeneratorForm();

        //get the form reference
        $patchTestTakersGeneratorForm = $PatchTestTakersGeneratorContainer->getForm();
        $Result = null;
        if ($patchTestTakersGeneratorForm->isSubmited() && $patchTestTakersGeneratorForm->isValid()) {
            $formGroupName = $patchTestTakersGeneratorForm->getValue('group_name');
            $formGroupNameForGenerator = preg_replace("/(?<=[a-zA-Z])(?=[A-Z])/", "_", $formGroupName);

            $formTestTakersCount = $patchTestTakersGeneratorForm->getValue('test_takers_count');

            $MainGroupClass = PatchGeneratorHelpers::checkIfClassGroupExist();
            $MainTestTakersClass = PatchGeneratorHelpers::checkIfClassTestTakersExist();

            $GroupName = preg_replace("/(?<=[a-zA-Z])(?=[A-Z])/", "_", $formGroupName) . '_' . date('Ymd_Hi') . '_patch_tt_generator';
            $TestTakersGroupName = preg_replace("/(?<=[a-zA-Z])(?=[A-Z])/", "_", $formGroupName) . '_' . date('Ymd_Hi') . '_patch_tt_generator';

            $groupClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_GROUP);

            $newGroup = $groupClass->createInstance($GroupName);
            $newGroup->editPropertyValues(new core_kernel_classes_Property('http://www.w3.org/1999/02/22-rdf-syntax-ns#type'), $MainGroupClass);


            $subTestTakersClass = $MainTestTakersClass->createSubClass($TestTakersGroupName);

            $UsersArray = [];

            if (is_numeric($formTestTakersCount)) {
                for ($i = 1; $i <= $formTestTakersCount; $i++) {
                    $password = PatchGeneratorHelpers::generateRandomString();
                    $TestTakerInstance = $this->createTestTaker($formGroupNameForGenerator, $password, $i, $subTestTakersClass);

                    $UserResource = UserHelper::getUser($TestTakerInstance);
                    $TestTakerLoginName = UserHelper::getUserLogin($UserResource);


                    $TestTakerInstance->setPropertyValue(new core_kernel_classes_Property(self::PROPERTY_MEMBERS_URI), $newGroup);

                    $UsersArray[] = array('uri' => $TestTakerInstance->getURI(), 'login-name' => $TestTakerLoginName, 'password' => $password);
                }
            }
            $json_data = json_encode($UsersArray);
            $JSONFileName = $FileDirectory . '/' . $TestTakersGroupName . '.json';

            if (!file_exists($FileDirectory)) {
                mkdir($FileDirectory, 0777, true);
            }

            file_put_contents($JSONFileName, $json_data);

            $CSVileName = $FileDirectory . '/' . $TestTakersGroupName . '.csv';
            PatchGeneratorHelpers::jsonToCSV($json_data, $CSVileName);

            $Result = 'Patch Generated Successfully (' . $GroupName . ')';
            $this->setData('message', __('Patch Generated Successfully'));


            $Query = [
                "structure" => $Extension,
                "ext" => $Extension,
                "section" => 'taoTestTakersPatchGenerator_main',
            ];


            $RedirectAfterSuccessful = ROOT_URL . "tao/Main/index?" . $this->URLGenerator($Query);

            $this->setData('redirectToMain', $RedirectAfterSuccessful);
        }

        $this->setData('formTitle', __('Patch Test-Takers Generator'));
        $this->setData('myForm', $patchTestTakersGeneratorForm->render());
        $this->setData('Result', $Result);
        $this->setView('TaoTestTakersPatchGenerator/testTakerForm.tpl');
    }


    private function createTestTaker($groupLabelName, $password, $counter, core_kernel_classes_Class $destinationClass)
    {
        $testTakerLabel = $groupLabelName . '-' . date('His') . '-' . $counter;
        $firstName = '';
        $lastName = '';
        $testTakerEmail = '';
        $testTakerUILanguage = tao_models_classes_LanguageService::singleton()->getLanguageByCode(DEFAULT_LANG)->getUri();
        $loginName = $testTakerLabel;
        $PlainPassword = $password;

        $itemClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_SUBJECT);
        $newItem = $itemClass->createInstance('New Test Taker Instance' . date("H:i:s Y-m-d"));
        $user = new core_kernel_classes_Resource($newItem->getURI());

        $values[OntologyRdfs::RDFS_LABEL] = $testTakerLabel;
        $values[UserRdf::PROPERTY_FIRSTNAME] = $firstName;
        $values[UserRdf::PROPERTY_LASTNAME] = $lastName;
        $values[UserRdf::PROPERTY_MAIL] = $testTakerEmail;
        $values[UserRdf::PROPERTY_UILG] = $testTakerUILanguage;
        $values[UserRdf::PROPERTY_LOGIN] = $loginName;
        $values[GenerisRdf::PROPERTY_USER_PASSWORD] = \core_kernel_users_Service::getPasswordHash()->encrypt($PlainPassword);

        $binder = new \tao_models_classes_dataBinding_GenerisFormDataBinder($user);
        $binder->bind($values);


        $tempValue = $this->getEventManager()->trigger(
            new UserUpdatedEvent(
                $user,
                $values
            )
        );

        $roleProperty = new \core_kernel_classes_Property(GenerisRdf::PROPERTY_USER_ROLES);
        $subjectRole = new \core_kernel_classes_Resource(TaoOntology::PROPERTY_INSTANCE_ROLE_DELIVERY);
        $user->setPropertyValue($roleProperty, $subjectRole);

        $user->editPropertyValues(new core_kernel_classes_Property('http://www.w3.org/1999/02/22-rdf-syntax-ns#type'), $destinationClass);

        return $user;
    }

    public function URLGenerator($array)
    {
        $URL = "";
        foreach ($array as $key => $value) {
            $URL .= $key . "=" . $value . '&amp;';
        }
        return $URL;
    }
}
