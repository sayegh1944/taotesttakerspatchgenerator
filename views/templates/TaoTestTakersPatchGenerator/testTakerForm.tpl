<?php

use oat\tao\helpers\Template;

Template::inc('form_context.tpl', 'tao');
?>


<header class="section-header flex-container-full">
    <h2><?= get_data('formTitle') ?></h2>
    <?php if (has_data('Result')) : ?>
        <h2 style="color:#1B5E20"><b><?= $Result ?></b></h2>
        <h3>You Will be redirected to patches page after 5 seconds</h3>
    <?php endif ?>
</header>



<div class="main-container flex-container-main-form">
    <div class="form-content">
        <?= get_data('myForm') ?>
    </div>
</div>
<?php
if (has_data('redirectToMain')) {
    echo '<script>setTimeout(function(){window.location.href = "'.get_data('redirectToMain').'" }, 5000)</script>';

}
?>
<?php Template::inc('footer.tpl', 'tao'); ?>
