<?php


namespace oat\taoTestTakersPatchGenerator\controller;

use tao_helpers_form_FormFactory;
use tao_helpers_form_FormContainer;
/**
 * This container initialize the settings form.
 *
 * @access public
 * @author 
 * @package tao
 * @subpackage actions_form
 */
class PatchTestTakersGeneratorForm extends \tao_helpers_form_FormContainer
{

    public function initForm()
    {
        $this->form = tao_helpers_form_FormFactory::getForm('login');

        $connectElt = tao_helpers_form_FormFactory::getElement('generate', 'Submit');
        $connectElt->setValue(__('Generate'));
        $this->form->setActions(array($connectElt), 'bottom');
    }

    public function initElements()
    {
        $groupNameElt = tao_helpers_form_FormFactory::getElement('group_name', 'Textbox');
        $groupNameElt->setDescription(__('(Group \ Label) name'));
        $groupNameElt->addValidator(tao_helpers_form_FormFactory::getValidator('NotEmpty'));
        $this->form->addElement($groupNameElt);

        $testTakersCountElt = tao_helpers_form_FormFactory::getElement('test_takers_count', 'Textbox');
        $testTakersCountElt->setDescription(__('Test Takers Count'));
        $testTakersCountElt->addValidator(tao_helpers_form_FormFactory::getValidator('NotEmpty'));
        $this->form->addElement($testTakersCountElt);

    }
}
