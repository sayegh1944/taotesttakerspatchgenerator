<?php


namespace oat\taoTestTakersPatchGenerator\controller;

use core_kernel_classes_Class;
use oat\tao\model\TaoOntology;
use tao_models_classes_LanguageService;
use core_kernel_classes_Resource;
use oat\generis\model\OntologyRdfs;
use oat\generis\model\user\UserRdf;
use oat\generis\model\GenerisRdf;
use core_kernel_users_Service;
use tao_models_classes_dataBinding_GenerisFormDataBinder;
use oat\tao\model\event\UserUpdatedEvent;
use core_kernel_classes_Property;
use tao_actions_RdfController;
use oat\generis\model\OntologyAwareTrait;
use oat\oatbox\event\EventManagerAwareTrait;
use tao_actions_SaSModule;
use tao_actions_CommonModule;

class PatchGeneratorHelpers  extends \tao_actions_CommonModule
{
    use OntologyAwareTrait;
    use EventManagerAwareTrait;

    public static $className = "Tao Test-takers Patch Generator";

    public static function checkIfClassGroupExist()
    {
        $PatchGeneratorClassExist = false;
        $subClassNeeded = null;
        $className = PatchGeneratorHelpers::$className;

        $groupClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_GROUP);
        $subClasses = $groupClass->getSubClasses(true);

        foreach ($subClasses as $subClass) {
            if ($subClass->getLabel() == $className) {
                $PatchGeneratorClassExist = true;
                $subClassNeeded = $subClass;
            }
        }

        if (!$PatchGeneratorClassExist) {
            $subClass = $groupClass->createSubClass($className);
            $subClassNeeded = $subClass;
        }

        return $subClassNeeded;
    }

    public static function checkIfClassTestTakersExist()
    {
        $PatchGeneratorClassExist = false;
        $className = PatchGeneratorHelpers::$className;
        $subClassNeeded = null;

        $groupClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_SUBJECT);
        $subClasses = $groupClass->getSubClasses(true);

        foreach ($subClasses as $subClass) {
            if ($subClass->getLabel() == $className) {
                $PatchGeneratorClassExist = true;
                $subClassNeeded = $subClass;
            }
        }

        if (!$PatchGeneratorClassExist) {
            $subClass = $groupClass->createSubClass($className);
            $subClassNeeded = $subClass;
        }

        return $subClassNeeded;
    }

    public static function generateRandomString($length = 8)
    {
        $characters = '0123456789wrfhxzWRFHLXZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        if (!preg_match('/[a-z]/', $randomString)) {
            $lowerCase = 'wrfhxz';
            $randomString .= $lowerCase[rand(0, strlen($lowerCase) - 1)];
        }
        if (!preg_match('/[A-Z]/', $randomString)) {
            $CapCase = 'WRFHLXZ';
            $randomString .= $CapCase[rand(0, strlen($CapCase) - 1)];
        }

        if (!preg_match('/[0-9]/', $randomString)) {
            $numericString = '0123456789';
            $randomString .= $numericString[rand(0, strlen($numericString) - 1)];
        }

        return $randomString;
    }

    public static function jsonToCSV($jsonData, $cfilename)
    {
        $data = json_decode($jsonData, true);
        $fp = fopen($cfilename, 'w');
        $header = false;
        foreach ($data as $row) {
            if (empty($header)) {
                $header = array_keys($row);
                fputcsv($fp, $header);
                $header = array_flip($header);
            }
            fputcsv($fp, array_merge($header, $row));
        }
        fclose($fp);
        return;
    }
}
